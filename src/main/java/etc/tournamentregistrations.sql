drop table tournamentregistrations;
create table tournamentregistrations
(
  tournamentId int NOT NULL,
  userId int NOT NULL,
  teamPoints float,
  teamName varchar(80),
  PRIMARY KEY CLUSTERED (tournamentId, userId)
);