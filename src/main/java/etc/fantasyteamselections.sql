drop table fantasyteamselections;
create table fantasyteamselections
(
  tournamentId int NOT NULL,
  userId int NOT NULL,
  playerId int NOT NULL,
  positionId int NOT NULL,
  PRIMARY KEY CLUSTERED (tournamentId, userId, playerId)
);