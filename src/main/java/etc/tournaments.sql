drop table tournaments;
create table tournaments
(
  id int Identity(1, 1) primary key NOT NULL,
  name varchar(80),
  startTime datetime,
  endTime datetime,
  leagueName varchar(80),
  status int
);