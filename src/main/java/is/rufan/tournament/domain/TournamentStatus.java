package is.rufan.tournament.domain;

/**
 * Created by Snaebjorn on 10/23/2015.
 */
public enum TournamentStatus {
    STATUS_ACTIVE,
    STATUS_INACTIVE,
    WINNER_SELECTED;

    public static TournamentStatus fromInt(int x) {
        switch (x) {
            case 0:
                return STATUS_ACTIVE;
            case 1:
                return STATUS_INACTIVE;
            case 2:
                return WINNER_SELECTED;
        }
        // default is inactive
        return STATUS_INACTIVE;
    }
}
