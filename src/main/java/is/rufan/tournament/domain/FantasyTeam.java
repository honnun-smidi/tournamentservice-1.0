package is.rufan.tournament.domain;

import is.rufan.player.domain.Player;
import is.rufan.player.domain.PlayerWithTeamName;
import is.rufan.tournament.data.FantasyTeamSelectionData;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO: document
 * Created by Omar on 24.10.2015.
 */
public class FantasyTeam {
    float points;
    int userId;
    String teamName;
    List<PlayerWithTeamName> players;
    Tournament tournament;

    public FantasyTeam() {

    }

    public FantasyTeam(int userId, Tournament tournament, String teamName) {
        this.userId = userId;
        this.points = 0;
        this.players = new ArrayList<PlayerWithTeamName>();
        this.tournament = tournament;
        this.teamName = teamName;
    }

    public List<PlayerWithTeamName> getPlayers() {
        return players;
    }

    public void setPlayers(List<PlayerWithTeamName> players) {
        this.players = players;
    }

    public float getPoints() {
        return points;
    }

    public void setPoints(float points) {
        this.points = points;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Tournament getTournament() {
        return tournament;
    }

    public void setTournament(Tournament tournament) {
        this.tournament = tournament;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    @Override
    public String toString() {
        return "FantasyTeam{" +
                "points=" + points +
                ", userId=" + userId +
                ", teamName='" + teamName + '\'' +
                ", players=" + players +
                ", tournament=" + tournament +
                '}';
    }
}