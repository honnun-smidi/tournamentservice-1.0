package is.rufan.tournament.domain;

import is.rufan.user.domain.User;

import java.util.Date;
import java.util.List;

/**
 * Created by Snaebjorn on 10/22/2015.
 */
public class Tournament {
    private int id;
    private String name;
    private Date startTime;
    private Date endTime;
    private String leagueName;
    private TournamentStatus status;
    private List<User> users;

    public Tournament() {
    }

    public Tournament(int id, String name, Date startTime, Date endTime, String leagueName, TournamentStatus status) {
        this.id = id;
        this.name = name;
        this.startTime = startTime;
        this.endTime = endTime;
        this.leagueName = leagueName;
        this.status = status;
    }

    public Tournament(String name, Date startTime, Date endTime, String leagueName, TournamentStatus status) {
        this.name = name;
        this.startTime = startTime;
        this.endTime = endTime;
        this.leagueName = leagueName;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getLeagueName() {
        return leagueName;
    }

    public void setLeagueName(String leagueName) {
        this.leagueName = leagueName;
    }

    public TournamentStatus getStatus() {
        return status;
    }

    public void setStatus(TournamentStatus status) {
        this.status = status;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public void addUser(User user) {
        users.add(user);
    }

    @Override
    public String toString() {
        return "Tournament{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", leagueName='" + leagueName + '\'' +
                ", status=" + status +
                '}';
    }
}
