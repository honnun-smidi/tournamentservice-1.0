package is.rufan.tournament.domain;

import is.rufan.player.domain.Player;

import java.util.List;

/**
 * Created by Snaebjorn on 10/25/2015.
 */
public class FantasyTeamListItem {
    float points;
    int userId;
    String teamName;
    Tournament tournament;

    public FantasyTeamListItem() {
    }

    public FantasyTeamListItem(float points, int userId, String teamName, Tournament tournament) {
        this.points = points;
        this.userId = userId;
        this.teamName = teamName;
        this.tournament = tournament;
    }

    public float getPoints() {
        return points;
    }

    public void setPoints(float points) {
        this.points = points;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public Tournament getTournament() {
        return tournament;
    }

    public void setTournament(Tournament tournament) {
        this.tournament = tournament;
    }

    @Override
    public String toString() {
        return "FantasyTeamListItem{" +
                "points=" + points +
                ", userId=" + userId +
                ", teamName='" + teamName + '\'' +
                ", tournament=" + tournament +
                '}';
    }
}
