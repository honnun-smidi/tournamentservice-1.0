package is.rufan.tournament.test;

import is.rufan.tournament.data.UserAlreadyRegisteredException;
import is.rufan.tournament.service.TournamentService;
import is.rufan.user.domain.User;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import java.util.List;

/**
 * Created by Snaebjorn on 10/24/2015.
 */
public class TestTournamentRegistrations {
    public static void main(String[] args) {
        new TestTournamentRegistrations();
    }

    public TestTournamentRegistrations() {
        ApplicationContext ctx = new FileSystemXmlApplicationContext("classpath:tournamentapp.xml");
        TournamentService tournamentService = (TournamentService) ctx.getBean("tournamentService");


        tournamentService.addUserToTournament(1, 1);
        tournamentService.addUserToTournament(1, 3);
        tournamentService.addUserToTournament(2, 1);
        tournamentService.addUserToTournament(2, 2);
        tournamentService.addUserToTournament(2, 3);
        tournamentService.addUserToTournament(2, 4);
        try {
            tournamentService.addUserToTournament(2, 4);
        } catch (UserAlreadyRegisteredException uarex) {
            System.out.println(uarex.getMessage());
        }

        List<User> usersInTourney1 = tournamentService.getUsersInTournament(1);
        List<User> usersInTourney2 = tournamentService.getUsersInTournament(2);

        System.out.println("Users in tournament 1");
        for (User u : usersInTourney1) {
            System.out.println(u);
        }

        System.out.println("Users in tournament 2");
        for (User u : usersInTourney2) {
            System.out.println(u);
        }
    }
}
