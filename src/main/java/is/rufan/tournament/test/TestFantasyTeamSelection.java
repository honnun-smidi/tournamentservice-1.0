package is.rufan.tournament.test;

import is.rufan.tournament.service.PlayerPositionException;
import is.rufan.tournament.service.FantasyTeamService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

/**
 * Created by Snaebjorn on 10/24/2015.
 */
public class TestFantasyTeamSelection {
    public static void main(String[] args) {
        new TestFantasyTeamSelection();
    }

    public TestFantasyTeamSelection() {
        ApplicationContext ctx = new FileSystemXmlApplicationContext("classpath:tournamentapp.xml");
        FantasyTeamService fantasyTeamService = (FantasyTeamService) ctx.getBean("fantasyTeamService");

        /* fantasyTeamService.addPlayerToFantasyTeam(1, 1, 345196, 4);
        fantasyTeamService.addPlayerToFantasyTeam(1, 1, 345565, 4);
        fantasyTeamService.addPlayerToFantasyTeam(1, 1, 345605, 4);
        fantasyTeamService.addPlayerToFantasyTeam(1, 1, 345829, 4);
        fantasyTeamService.addPlayerToFantasyTeam(1, 1, 345199, 3);
        fantasyTeamService.addPlayerToFantasyTeam(1, 1, 345535, 3);
        fantasyTeamService.addPlayerToFantasyTeam(1, 1, 345762, 3);
        fantasyTeamService.addPlayerToFantasyTeam(1, 1, 479360, 3);
        fantasyTeamService.addPlayerToFantasyTeam(1, 1, 436213, 2);
        fantasyTeamService.addPlayerToFantasyTeam(1, 1, 580910, 2);
        fantasyTeamService.addPlayerToFantasyTeam(1, 1, 347350, 1); */


        /* List<Player> players = fantasyTeamService.getPlayersInFantasyTeam(1, 1);

        System.out.println("Players in fantasy team before delete");
        for (Player p : players) {
            System.out.println(p);
        }

        // fantasyTeamService.removePlayerFromFantasyTeam(1, 1, 274190);

        System.out.println("Players in fantasy team after delete");
        for (Player p : players) {
            System.out.println(p);
        } */

        // FantasyTeam getFantasyTeam = fantasyTeamService.getFantasyTeam(1, 1);
        try {
           // fantasyTeamService.addPlayerToFantasyTeam(1, 1, 407957, 4);
            //fantasyTeamService.addPlayerToFantasyTeam(1, 1, 274190, 1);
            //fantasyTeamService.addPlayerToFantasyTeam(1, 1, 423401, 3);
            fantasyTeamService.addPlayerToFantasyTeam(1, 1, 435987, 2);
        } catch (PlayerPositionException ppex) {
            System.out.println(ppex.getMessage());
        }

        System.out.println("Fantasy team for user 3 in tournament 1");
        // System.out.println(getFantasyTeam);
        // System.out.println("Player count: " + getFantasyTeam.getPlayers().size());


    }
}
