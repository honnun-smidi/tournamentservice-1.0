package is.rufan.tournament.test;

import is.rufan.tournament.data.TournamentData;
import is.rufan.tournament.domain.Tournament;
import is.rufan.tournament.domain.TournamentStatus;
import is.rufan.tournament.service.TournamentService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import java.util.Date;
import java.util.List;

/**
 * Created by Snaebjorn on 10/22/2015.
 */
public class TestTournament {
    public static void main(String[] args) {
        new TestTournament();
    }

    public TestTournament() {
        ApplicationContext ctx = new FileSystemXmlApplicationContext("classpath:tournamentapp.xml");
        TournamentService tournamentService = (TournamentService) ctx.getBean("tournamentService");
        /* int index = tournamentService.addTournament(
                new Tournament("League of Legends", new Date(), new Date(),
                        "EPL", TournamentStatus.STATUS_ACTIVE));
        tournamentService.addTournament(
                new Tournament("Real Mf Gs", new Date(), new Date(),
                        "EPL", TournamentStatus.STATUS_ACTIVE));
        tournamentService.addTournament(
                new Tournament("Mudders from Hell", new Date(), new Date(),
                        "EPL", TournamentStatus.STATUS_INACTIVE));

        Tournament tournament = tournamentService.getTournament(index);
        System.out.println(tournament); */

        List<Tournament> activeTournaments = tournamentService.getTournamentsByStatus(TournamentStatus.STATUS_ACTIVE);
        List<Tournament> inactiveTournaments = tournamentService.getTournamentsByStatus(TournamentStatus.STATUS_INACTIVE);

        System.out.println("Active tournaments");
        for (Tournament t : activeTournaments) {
            System.out.println(t);
        }

        System.out.println("Inactive tournaments");
        for (Tournament t : inactiveTournaments) {
            System.out.println(t);
        }

        List<Tournament> tournamentsForUser = tournamentService.getTournamentsForUser(3);

        System.out.println("User 3 is participating in tournaments");
        for (Tournament t : tournamentsForUser) {
            System.out.println(t);
        }
    }
}
