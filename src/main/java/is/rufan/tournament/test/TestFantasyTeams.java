package is.rufan.tournament.test;

import is.rufan.tournament.domain.FantasyTeamListItem;
import is.rufan.tournament.service.FantasyTeamService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import java.util.List;

/**
 * Created by Snaebjorn on 10/25/2015.
 */
public class TestFantasyTeams {
    public static void main(String[] args) {
        new TestFantasyTeams();
    }

    public TestFantasyTeams() {
        ApplicationContext ctx = new FileSystemXmlApplicationContext("classpath:tournamentapp.xml");
        FantasyTeamService fantasyTeamService = (FantasyTeamService) ctx.getBean("fantasyTeamService");

        int playerId = 3;
        fantasyTeamService.updateFantasyTeamName(3, 1, "Bingo buddies");
        List<FantasyTeamListItem> fantasyTeamListItems = fantasyTeamService.getActiveFantasyTeamList(playerId);

        System.out.println("Fantasy teams for player " + playerId);
        for (FantasyTeamListItem f : fantasyTeamListItems) {
            System.out.println(f);
        }
    }
}
