package is.rufan.tournament.service;

/**
 * This exception gets thrown when a fantasy team already has the maximum players in the
 * position the player is being added to.
 * Created by Omar on 26.10.2015.
 */
public class PlayerPositionException extends RuntimeException {
    public PlayerPositionException() {

    }

    public PlayerPositionException(String message ) {
        super(message);
    }

    public PlayerPositionException(String message, Throwable cause) {
        super(message, cause);
    }
}
