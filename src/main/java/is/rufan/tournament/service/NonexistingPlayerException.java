package is.rufan.tournament.service;

/**
 * This exception gets thrown when either no player with playerId is found, or
 * he does not have the position that he is supposed to fill.
 * Created by Snaebjorn on 10/26/2015.
 */
public class NonexistingPlayerException extends RuntimeException {
    public NonexistingPlayerException() {
    }

    public NonexistingPlayerException(String message) {
        super(message);
    }

    public NonexistingPlayerException(String message, Throwable cause) {
        super(message, cause);
    }
}
