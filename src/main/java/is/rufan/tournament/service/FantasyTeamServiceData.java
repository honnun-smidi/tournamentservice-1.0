package is.rufan.tournament.service;

import is.rufan.player.domain.Player;
import is.rufan.player.domain.PlayerWithTeamName;
import is.rufan.player.domain.Position;
import is.rufan.player.service.PlayerService;
import is.rufan.tournament.data.FantasyTeamSelectionDataGateway;
import is.rufan.tournament.data.TournamentRegistrationDataGateway;
import is.rufan.tournament.domain.FantasyTeam;
import is.rufan.tournament.domain.FantasyTeamListItem;
import is.ruframework.data.RuDataAccessFactory;
import is.ruframework.domain.RuException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import java.util.List;

/**
 * Handles the business logic for fantasy teams using an SQL database.
 * Fantasy teams are identified by a user ID and tournament ID - since
 * each user can have only one fantasy team per tournament.
 * Created by Snaebjorn on 10/24/2015.
 */
public class FantasyTeamServiceData implements FantasyTeamService {
    private RuDataAccessFactory factory;
    private FantasyTeamSelectionDataGateway fantasyTeamSelectionDataGateway;
    private TournamentRegistrationDataGateway tournamentRegistrationDataGateway;
    private PlayerService playerService;

    public FantasyTeamServiceData() throws RuException {
        // get access to database
        factory = RuDataAccessFactory.getInstance("tournamentdata.xml");
        fantasyTeamSelectionDataGateway = (FantasyTeamSelectionDataGateway)
                factory.getDataAccess("fantasyTeamSelectionData");
        tournamentRegistrationDataGateway = (TournamentRegistrationDataGateway)
                factory.getDataAccess("tournamentRegistrationData");
        ApplicationContext ctx = new FileSystemXmlApplicationContext("classpath:playerapp.xml");
        playerService = (PlayerService) ctx.getBean("playerService");
    }

    /**
     * Add the player with the given playerId to the user with the given userId's fantasy team
     * participating in the tournament with the given tournamentId.
     * @param userId
     * @param tournamentId
     * @param playerId
     */
    public void addPlayerToFantasyTeam(int userId, int tournamentId, int playerId, int positionId) {
        // check if player exists and his positions include the positionId
        Player player = playerService.getPlayer(playerId);
        if (player == null)
            throw new NonexistingPlayerException("No player found with playerId");

        boolean playerHasPosition = false;
        for (Position p : player.getPositions()) {
            if (p.getPositionId() == positionId) {
                playerHasPosition = true;
                break;
            }
        }
        if (!playerHasPosition) {
            throw new NonexistingPlayerException("The positionId does not match the player's positions");
        }

        List<PlayerWithTeamName> playersInTeam = getPlayersInFantasyTeam(userId, tournamentId);
        int playersInPositionCount = countPlayersInPosition(positionId, playersInTeam);

        if (positionId == 1 && playersInPositionCount >= 1) {
            throw new PlayerPositionException("You already have a goalkeeper");
        } else if (positionId == 2 && playersInPositionCount >= 2) {
            throw new PlayerPositionException("You already have two forwards");
        } else if (positionId == 3 && playersInPositionCount >= 4) {
            throw new PlayerPositionException("You already have four midfielders");
        } else if (positionId == 4 && playersInPositionCount >= 4) {
            throw new PlayerPositionException("You already have four defenders");
        }

        fantasyTeamSelectionDataGateway.addPlayerToFantasyTeam(userId, tournamentId, playerId, positionId);
    }

    /**
     * Counts players playing in this position
     * @param positionId
     * @param players
     * @return
     */
    private int countPlayersInPosition(int positionId, List<PlayerWithTeamName> players) {
        int count = 0;

        for (Player p : players) {
            Position pos = p.getPositions().get(0);
            if (pos.getPositionId() == positionId) {
                count++;
            }
        }

        return count;
    }

    /**
     * Remove the player with the given playerId to the user with the given userId's fantasy team
     * participating in the tournament with the given tournamentId.
     * @param userId
     * @param tournamentId
     * @param playerId
     */
    public void removePlayerFromFantasyTeam(int userId, int tournamentId, int playerId) {
        fantasyTeamSelectionDataGateway.removePlayerFromFantasyTeam(userId, tournamentId, playerId);
    }

    /**
     * Returns the players which the user with the given userId has selected for his
     * fantasy team participating in the tournament with the given tournamentId
     * @param userId
     * @param tournamentId
     * @return
     */
    public List<PlayerWithTeamName> getPlayersInFantasyTeam(int userId, int tournamentId) {
        return fantasyTeamSelectionDataGateway.getPlayersInFantasyTeam(userId, tournamentId);
    }

    /**
     * Returns the fantasy team which the user with the given userId has
     * selected to participate in the tournament with the selected
     * tournamentId.
     * @param userId
     * @param tournamentId
     * @return
     */
    public FantasyTeam getFantasyTeam(int userId, int tournamentId) {
        return fantasyTeamSelectionDataGateway.getFantasyTeam(userId, tournamentId);
    }

    /**
     * Returns a list of fantasy teams currently active for the user
     * with the given userId
     * @param userId
     * @return
     */
    public List<FantasyTeamListItem> getActiveFantasyTeamList(int userId) {
        return tournamentRegistrationDataGateway.getActiveFantasyTeams(userId);
    }

    public void updateFantasyTeamPoints(int userId, int tournamentId, float points) {
        // TODO: update fantasy team points
        tournamentRegistrationDataGateway.updateFantasyTeamPoints(userId, tournamentId, points);
    }

    public void updateFantasyTeamName(int userId, int tournamentId, String name) {
        tournamentRegistrationDataGateway.updateFantasyTeamName(userId, tournamentId, name);
    }
}
