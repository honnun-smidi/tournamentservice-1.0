package is.rufan.tournament.service;

import is.rufan.tournament.data.TournamentDataGateway;
import is.rufan.tournament.data.TournamentRegistrationDataGateway;
import is.rufan.tournament.data.UserAlreadyRegisteredException;
import is.rufan.tournament.domain.Tournament;
import is.rufan.tournament.domain.TournamentStatus;
import is.rufan.user.domain.User;
import is.ruframework.data.RuDataAccessFactory;
import is.ruframework.domain.RuException;

import java.util.List;

/**
 *
 * Created by Snaebjorn on 10/23/2015.
 */
public class TournamentServiceData implements TournamentService {
    private RuDataAccessFactory factory;
    private TournamentDataGateway tournamentDataGateway;
    private TournamentRegistrationDataGateway tournamentRegistrationDataGateway;

    public TournamentServiceData() throws RuException {
        factory = RuDataAccessFactory.getInstance("tournamentdata.xml");
        tournamentDataGateway = (TournamentDataGateway)
                factory.getDataAccess("tournamentData");
        tournamentRegistrationDataGateway = (TournamentRegistrationDataGateway)
                factory.getDataAccess("tournamentRegistrationData");
    }

    public int addTournament(Tournament tournament) {
        return tournamentDataGateway.addTournament(tournament);
    }

    public Tournament getTournament(int tournamentId) {
        return tournamentDataGateway.getTournamentById(tournamentId);
    }

    public List<Tournament> getTournaments() {
        return tournamentDataGateway.getTournaments();
    }

    public List<Tournament> getTournamentsByStatus(TournamentStatus status) {
        return tournamentDataGateway.getTournamentsByStatus(status);
    }

    public void addUserToTournament(int tournamentId, int userId) {
        tournamentRegistrationDataGateway.addUserToTournament(tournamentId, userId);
    }

    public List<User> getUsersInTournament(int tournamentId) {
        return tournamentRegistrationDataGateway.getUsersInTournament(tournamentId);
    }

    public List<Tournament> getTournamentsForUser(int userId) {
        return tournamentDataGateway.getTournamentsForUser(userId);
    }

    public void updateTournamentStatus(int tournamentId, TournamentStatus status) {
        tournamentDataGateway.updateTournamentStatus(tournamentId, status);
    }
}
