package is.rufan.tournament.service;

import is.rufan.tournament.data.UserAlreadyRegisteredException;
import is.rufan.tournament.domain.Tournament;
import is.rufan.tournament.domain.TournamentStatus;
import is.rufan.user.domain.User;

import java.util.List;

/**
 * Handles the business logic for tournaments
 * Created by Snaebjorn on 10/23/2015.
 */
public interface TournamentService {
    int addTournament(Tournament tournament);
    Tournament getTournament(int tournamentId);
    List<Tournament> getTournaments();
    List<Tournament> getTournamentsByStatus(TournamentStatus status);
    void updateTournamentStatus(int tournamentId, TournamentStatus status);
    void addUserToTournament(int tournamentId, int userId);
    List<User> getUsersInTournament(int tournamentId);
    List<Tournament> getTournamentsForUser(int userId);
}
