package is.rufan.tournament.service;

import is.rufan.player.domain.Player;
import is.rufan.player.domain.PlayerWithTeamName;
import is.rufan.tournament.domain.FantasyTeam;
import is.rufan.tournament.domain.FantasyTeamListItem;

import java.util.List;

/**
 * Handles the business logic for fantasy teams using an SQL database.
 * Fantasy teams are identified by a user ID and tournament ID - since
 * each user can have only one fantasy team per tournament.
 * Created by Snaebjorn on 10/24/2015.
 */
public interface FantasyTeamService {
    void addPlayerToFantasyTeam(int userId, int tournamentId, int playerId, int positionId);
    void removePlayerFromFantasyTeam(int userId, int tournamentId, int playerId);
    void updateFantasyTeamPoints(int userId, int tournamentId, float points);
    void updateFantasyTeamName(int userId, int tournamentId, String name);
    List<PlayerWithTeamName> getPlayersInFantasyTeam(int userId, int tournamentId);
    FantasyTeam getFantasyTeam(int userId, int tournamentId);
    List<FantasyTeamListItem> getActiveFantasyTeamList(int userId);
}
