package is.rufan.tournament.process;

import is.rufan.player.domain.Player;
import is.rufan.player.service.PlayerService;
import is.rufan.tournament.domain.FantasyTeam;
import is.rufan.tournament.domain.Tournament;
import is.rufan.tournament.domain.TournamentStatus;
import is.rufan.tournament.service.FantasyTeamService;
import is.rufan.tournament.service.TournamentService;
import is.rufan.user.domain.User;
import is.ruframework.process.RuAbstractProcess;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import java.util.List;
import java.util.logging.Logger;

/**
 * This process fetches the players in the tournament and their respective fantasy teams.
 * It then accumulates the fantasy points for the players in their team by accessing the
 * records which FantasyPointsImportProcess in PlayerService has already loaded into the database.
 * Created by Snaebjorn on 10/27/2015.
 */
public class WinnerSelectionProcess extends RuAbstractProcess {
    private PlayerService playerService;
    private TournamentService tournamentService;
    private FantasyTeamService fantasyTeamService;
    private Tournament tournament;
    Logger log = Logger.getLogger(this.getClass().getName());

    @Override
    public void beforeProcess() {
        ApplicationContext playerCtx = new FileSystemXmlApplicationContext("classpath:playerapp.xml");
        ApplicationContext tournamentCtx = new FileSystemXmlApplicationContext("classpath:tournamentapp.xml");
        playerService = (PlayerService) playerCtx.getBean("playerService");
        tournamentService = (TournamentService) tournamentCtx.getBean("tournamentService");
        fantasyTeamService = (FantasyTeamService) tournamentCtx.getBean("fantasyTeamService");

        int tournamentId = (Integer) getProcessContext().getParams().get("tournamentId");
        tournament = tournamentService.getTournament(tournamentId);
        // the tournament's status must be STATUS_ACTIVE
        if (tournament.getStatus() != TournamentStatus.STATUS_ACTIVE) {
            log.warning("This tournament's status is not STATUS_ACTIVE");
            System.exit(1);
        }
        log.info("processbefore" + getProcessContext().getProcessName());
    }

    @Override
    public void afterProcess() {
        log.info("processafter");
        tournamentService.updateTournamentStatus(tournament.getId(), TournamentStatus.WINNER_SELECTED);
    }

    @Override
    public void startProcess() {
        System.out.println("Tournament: " + tournament);

        List<User> usersInTournament = tournamentService.getUsersInTournament(tournament.getId());
        for (User u : usersInTournament) {
            float fantasyTeamPoints = calculateFantasyTeamPoints(u.getId(), tournament.getId());
            // update the user's fantasy team points
            fantasyTeamService.updateFantasyTeamPoints(u.getId(), tournament.getId(), fantasyTeamPoints);
        }
    }

    /**
     * Accumulates the fantasy points of all the player's in the fantasy team
     * which the user with the given userId has selected for this tournament.
     * @param userId
     * @param tournamentId
     * @return
     */
    private float calculateFantasyTeamPoints(int userId, int tournamentId) {
        FantasyTeam fantasyTeam = fantasyTeamService.getFantasyTeam(userId, tournamentId);
        float totalTeamPoints = 0;

        for (Player p : fantasyTeam.getPlayers()) {
            totalTeamPoints += playerService.getPlayerFantasyPoints(p.getPlayerId()).getFantasyPoints();
        }

        System.out.println("Total Team Points for user with id: " + userId + " = " + totalTeamPoints);

        return totalTeamPoints;
    }
}
