package is.rufan.tournament.data;

import is.rufan.player.domain.Player;
import is.rufan.player.service.PlayerService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Row mapper which fetches the player from the playerService,
 * this may be fast enough since a fantasy team can only have
 * 11 players.
 * Created by Snaebjorn on 10/24/2015.
 */
public class FantasyTeamSelectionRowMapper implements RowMapper<Player> {
    private PlayerService playerService;

    public FantasyTeamSelectionRowMapper() {
        ApplicationContext ctx = new FileSystemXmlApplicationContext("classpath:playerapp.xml");
        playerService = (PlayerService) ctx.getBean("playerService");
    }

    public Player mapRow(ResultSet rs, int i) throws SQLException {
        return playerService.getPlayer(rs.getInt("playerId"));
    }
}
