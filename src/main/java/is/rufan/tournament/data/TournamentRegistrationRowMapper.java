package is.rufan.tournament.data;

import is.rufan.user.domain.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Row mapper which returns a user with his essential information,
 * without his credit card number or favorite team.
 * Created by Snaebjorn on 10/24/2015.
 */
public class TournamentRegistrationRowMapper implements RowMapper<User> {
    public User mapRow(ResultSet rs, int i) throws SQLException {
        User user = new User();
        user.setId(rs.getInt("userId"));
        user.setName(rs.getString("name"));
        user.setUsername(rs.getString("username"));
        user.setEmail(rs.getString("email"));
        // credit card and favorite team are skipped because they are not really needed
        return user;
    }
}
