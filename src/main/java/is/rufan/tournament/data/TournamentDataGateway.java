package is.rufan.tournament.data;

import is.rufan.tournament.domain.Tournament;
import is.rufan.tournament.domain.TournamentStatus;
import is.ruframework.data.RuDataAccess;

import java.util.List;

/**
 * Accesses the tournament records, that is tournaments
 * which user's participate in and select fantasy teams for.
 * Created by Snaebjorn on 10/22/2015.
 */
public interface TournamentDataGateway {
    int addTournament(Tournament tournament);
    Tournament getTournamentById(int id);
    List<Tournament> getTournaments();
    List<Tournament> getTournamentsByStatus(TournamentStatus status);
    List<Tournament> getTournamentsForUser(int userId);
    void updateTournamentStatus(int tournamentId, TournamentStatus status);
}
