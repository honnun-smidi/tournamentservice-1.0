package is.rufan.tournament.data;

/**
 * Gets thrown when a user is already registered in a tournament.
 * Created by Snaebjorn on 10/24/2015.
 */
public class UserAlreadyRegisteredException extends RuntimeException {
    public UserAlreadyRegisteredException() {
    }

    public UserAlreadyRegisteredException(String message) {
        super(message);
    }

    public UserAlreadyRegisteredException(String message, Throwable cause) {
        super(message, cause);
    }
}
