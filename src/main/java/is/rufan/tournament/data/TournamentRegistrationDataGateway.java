package is.rufan.tournament.data;

import is.rufan.tournament.domain.FantasyTeamListItem;
import is.rufan.tournament.domain.Tournament;
import is.rufan.user.domain.User;

import java.util.List;

/**
 * Accesses the tournament registration records, i.e. which users
 * are registered in which tournaments.
 * Created by Snaebjorn on 10/24/2015.
 */
public interface TournamentRegistrationDataGateway {
    void addUserToTournament(int tournamentId, int userId) throws UserAlreadyRegisteredException;
    List<User> getUsersInTournament(int tournamentId);
    List<FantasyTeamListItem> getActiveFantasyTeams(int userId);
    void updateFantasyTeamPoints(int userId, int tournamentId, float points);
    void updateFantasyTeamName(int userId, int tournamentId, String name);
}
