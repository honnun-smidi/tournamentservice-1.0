package is.rufan.tournament.data;

import is.rufan.player.domain.Player;
import is.rufan.player.domain.PlayerWithTeamName;
import is.rufan.tournament.domain.FantasyTeam;

import java.util.List;

/**
 * Accesses the records connecting player's to fantasy teams -
 * which are identified by user's IDs and tournament's IDs
 * Created by Snaebjorn on 10/24/2015.
 */
public interface FantasyTeamSelectionDataGateway {
    void addPlayerToFantasyTeam(int userId, int tournamentId, int playerId, int positionId);
    void removePlayerFromFantasyTeam(int userId, int tournamentId, int playerId);
    List<PlayerWithTeamName> getPlayersInFantasyTeam(int userId, int tournamentId);
    FantasyTeam getFantasyTeam(int userId, int tournamentId);
}
