package is.rufan.tournament.data;

import is.rufan.tournament.domain.Tournament;
import is.rufan.tournament.domain.TournamentStatus;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Row mapper which returns a tournament with
 * all of its information.
 * Created by Snaebjorn on 10/22/2015.
 */
public class TournamentRowMapper implements RowMapper<Tournament> {
    public Tournament mapRow(ResultSet rs, int i) throws SQLException {
        Tournament tournament = new Tournament();
        tournament.setId(rs.getInt("id"));
        tournament.setName(rs.getString("name"));
        tournament.setStartTime(rs.getDate("startTime"));
        tournament.setEndTime(rs.getDate("endTime"));
        tournament.setLeagueName(rs.getString("leagueName"));
        // get tournament status from table - instead of casting int to enum (expensive operation),
        // we compare the column int value with the status active integer value
        // code horror dude is overruled by bad performance dude
        int tournamentStatusInt = rs.getInt("status");

        TournamentStatus tournamentStatus = TournamentStatus.fromInt(tournamentStatusInt);

        tournament.setStatus(tournamentStatus);

        return tournament;
    }
}
