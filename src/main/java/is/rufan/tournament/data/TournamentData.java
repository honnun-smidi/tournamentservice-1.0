package is.rufan.tournament.data;

import is.rufan.tournament.domain.Tournament;
import is.rufan.tournament.domain.TournamentStatus;
import is.ruframework.data.RuData;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Accesses the tournament records, that is tournaments
 * which user's participate in and select fantasy teams for.
 * Created by Snaebjorn on 10/22/2015.
 */
public class TournamentData extends RuData implements TournamentDataGateway {
    /**
     * Adds a tournament to the database and returns the database generated
     * unique identifier.
     * @param tournament
     * @return
     */
    public int addTournament(Tournament tournament) {
        SimpleJdbcInsert insertTournament =
                new SimpleJdbcInsert(getDataSource())
                        .withTableName("tournaments")
                        .usingGeneratedKeyColumns("id");

        Map<String, Object> parameters = new HashMap<String, Object>(5);
        parameters.put("name", tournament.getName());
        parameters.put("startTime", tournament.getStartTime());
        parameters.put("endTime", tournament.getEndTime());
        parameters.put("leagueName", tournament.getLeagueName());
        parameters.put("status", tournament.getStatus().ordinal());

        int returnKey = 0;
        try {
            returnKey = insertTournament.executeAndReturnKey(parameters).intValue();
        } catch (DataIntegrityViolationException divex) {
            log.warning(divex.getMessage());
        }

        return returnKey;
    }

    /**
     * Returns the tournament with the given id.
     * @param id
     * @return
     */
    public Tournament getTournamentById(int id) {
        String sql = "select * from tournaments where id = ?";
        JdbcTemplate queryTournament = new JdbcTemplate(getDataSource());
        Tournament tournament = queryTournament.queryForObject(sql, new Object[] { id },
                new TournamentRowMapper());
        return tournament;
    }

    /**
     * Returns all tournaments in the database, regardless of status.
     * @return
     */
    public List<Tournament> getTournaments() {
        String sql = "select * from tournaments";
        JdbcTemplate queryTournaments = new JdbcTemplate(getDataSource());
        List<Tournament> tournaments = queryTournaments.query(sql,
                new TournamentRowMapper());

        return tournaments;
    }

    /**
     * Returns tournaments by status, e.g. only tournaments
     * with the TournamentStatus STATUS_ACTIVE
     * @param status
     * @return
     */
    public List<Tournament> getTournamentsByStatus(TournamentStatus status) {
        String sql = "select * from tournaments where status = ?";
        JdbcTemplate queryTournaments = new JdbcTemplate(getDataSource());
        List<Tournament> tournaments = queryTournaments.query(sql, new Object[] { status.ordinal() },
                new TournamentRowMapper());

        return tournaments;
    }

    /**
     * Returns tournaments which the user is participating in.
     * @param userId
     * @return
     */
    public List<Tournament> getTournamentsForUser(int userId) {
        String sql = "select * from tournamentregistrations tr "
                + "join tournaments t on tr.tournamentid = t.id "
                + "where tr.userid = ? "
                + "and t.status = 0";
        JdbcTemplate queryTournaments = new JdbcTemplate(getDataSource());
        List<Tournament> tournaments = queryTournaments.query(sql, new Object[] { userId },
                new TournamentRowMapper());

        return tournaments;
    }

    public void updateTournamentStatus(int tournamentId, TournamentStatus status) {
        String sql = "update tournaments " +
                    "set status = ? " +
                    "where id = ?";
        JdbcTemplate updateTournamentStatus = new JdbcTemplate(getDataSource());
        updateTournamentStatus.update(sql, new Object[] { status.ordinal(), tournamentId });
    }
}
