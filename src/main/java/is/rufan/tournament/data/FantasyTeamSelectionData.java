package is.rufan.tournament.data;

import is.rufan.player.data.PlayerPositionRowMapper;
import is.rufan.player.domain.Player;
import is.rufan.player.domain.PlayerWithTeamName;
import is.rufan.tournament.domain.FantasyTeam;
import is.rufan.tournament.domain.Tournament;
import is.ruframework.data.RuData;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This data class interacts with the fantasyteamselections table,
 * which stores the player's that have been selected for a given
 * fantasy team. Fantasy teams are identified by the userId and
 * tournament - each user can have at most one fantasy team per tournament.
 * Created by Snaebjorn on 10/24/2015.
 */
public class FantasyTeamSelectionData extends RuData implements FantasyTeamSelectionDataGateway {
    /**
     * Add a record to the table, claiming that the player with
     * the given playerId - playing in the position with the given positionId
     * has been selected by the user with the given userId for his fantasy team
     * in the tournament with the given tournament id.
     * @param userId
     * @param tournamentId
     * @param playerId
     */
    public void addPlayerToFantasyTeam(int userId, int tournamentId, int playerId, int positionId) {
        SimpleJdbcInsert insertFantasyTeamSelection =
                new SimpleJdbcInsert(getDataSource())
                    .withTableName("fantasyteamselections");

        Map<String, Object> parameters = new HashMap<String, Object>(4);
        parameters.put("tournamentId", tournamentId);
        parameters.put("userId", userId);
        parameters.put("playerId", playerId);
        parameters.put("positionId", positionId);

        try {
            insertFantasyTeamSelection.execute(parameters);
        } catch (DataIntegrityViolationException divex) {
            String msg = "Player already selected for team";
            log.warning(msg);
            throw new PlayerAlreadySelectedException(msg);
        }
    }

    /**
     * Remove a record from the table which claims that the player with
     * the given playerId has been selected by the user with the
     * given userId for his fantasy team in the tournament with the
     * given tournament id.
     * @param userId
     * @param tournamentId
     * @param playerId
     */
    public void removePlayerFromFantasyTeam(int userId, int tournamentId, int playerId) {
        String sql = "delete from fantasyteamselections " +
                    "where userId = ? and tournamentId = ? and playerId = ?";

        JdbcTemplate deletePlayerFromFantasyTeam = new JdbcTemplate(getDataSource());
        deletePlayerFromFantasyTeam.update(sql,
                new Object[]{userId, tournamentId, playerId});
    }

    /**
     * Fetches the id's of the players that have been selected for
     * the given fantasy team.
     * @param userId
     * @param tournamentId
     * @return
     */
    public List<PlayerWithTeamName> getPlayersInFantasyTeam(int userId, int tournamentId) {
        String sql = "select p.playerid, p.firstname, p.lastname, p.height, " +
                    "p.weight, p.birthdate, p.teamid, c.countryid, " +
                    "c.name AS countryname, c.abbreviation AS countryabbreviation, " +
                    "po.positionid, po.name AS positionname, " +
                    "po.abbreviation AS positionabbreviation, t.displayname AS teamname " +
                    "from fantasyteamselections fts " +
                    "join players p on fts.playerid = p.playerid " +
                    "join countries c on p.countryid = c.countryid " +
                    "join positions po on fts.positionid = po.positionid " +
                    "join teams t on p.teamid = t.teamid " +
                    "where fts.userId = ? and fts.tournamentId = ?";
        JdbcTemplate queryPlayersInFantasyTeam = new JdbcTemplate(getDataSource());
        // use the PlayerPosition row mapper from PlayerService, because we are only concerned
        // with players with the position they were selected for in the team. E.g. Giorgino Wijnaldum
        // as a midfielder, not as a forward.
        List<PlayerWithTeamName> players = queryPlayersInFantasyTeam.query(sql,
                new Object[] { userId, tournamentId }, new PlayerPositionRowMapper());

        return players;
    }

    /**
     * Fetches the tournament and tournament registration for the user.
     * Finally creates and returns the user's fantasy team in this tournament
     * @param userId
     * @param tournamentId
     * @return
     */
    public FantasyTeam getFantasyTeam(int userId, int tournamentId) {
        // TODO: get tournament information and then get the players
        // TODO: create fantasy team object and return
        String tournamentRegistrationSql =
                    "select * from tournamentregistrations tr "
                  + "join tournaments t on tr.tournamentId = t.id "
                  + "where tr.tournamentid = ? and tr.userId = ?";

        JdbcTemplate queryFantasyTeam = new JdbcTemplate(getDataSource());
        FantasyTeam fantasyTeam = queryFantasyTeam.queryForObject(tournamentRegistrationSql,
                new Object[] { tournamentId, userId }, new FantasyTeamRowMapper());

        List<PlayerWithTeamName> players = getPlayersInFantasyTeam(userId, tournamentId);
        fantasyTeam.setPlayers(players);

        return fantasyTeam;
    }
}
