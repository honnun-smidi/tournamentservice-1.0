package is.rufan.tournament.data;

import is.rufan.tournament.domain.FantasyTeamListItem;
import is.rufan.tournament.domain.Tournament;
import is.rufan.tournament.domain.TournamentStatus;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * This mapper returns a FantasyTeamListItem, which contains information about the fantasy team -
 * but does not contain the fantasy team's players.
 * Created by Snaebjorn on 10/25/2015.
 */
public class FantasyTeamListItemRowMapper implements RowMapper<FantasyTeamListItem> {
    public FantasyTeamListItem mapRow(ResultSet rs, int i) throws SQLException {
        FantasyTeamListItem fantasyTeam = new FantasyTeamListItem();
        fantasyTeam.setUserId(rs.getInt("userId"));
        String teamName = (rs.getString("teamName") == null)
                ? "No name"
                : rs.getString("teamName");
        fantasyTeam.setTeamName(teamName);
        fantasyTeam.setPoints(rs.getFloat("teamPoints"));

        Tournament tournament = new Tournament();
        tournament.setId(rs.getInt("tournamentid"));
        tournament.setName(rs.getString("name"));
        tournament.setStartTime(rs.getDate("startTime"));
        tournament.setEndTime(rs.getDate("endTime"));
        tournament.setLeagueName(rs.getString("leagueName"));
        tournament.setStatus(TournamentStatus.fromInt(rs.getInt("status")));

        fantasyTeam.setTournament(tournament);
        return fantasyTeam;
    }
}
