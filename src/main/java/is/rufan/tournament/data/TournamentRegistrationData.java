package is.rufan.tournament.data;

import is.rufan.tournament.domain.FantasyTeamListItem;
import is.rufan.tournament.domain.Tournament;
import is.rufan.tournament.domain.TournamentStatus;
import is.rufan.user.domain.User;
import is.ruframework.data.RuData;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** Accesses the tournament registration records, i.e. which users
 *  are registered in which tournaments.
 *  Created by Snaebjorn on 10/24/2015.
 */
public class TournamentRegistrationData extends RuData implements TournamentRegistrationDataGateway {
    /**
     * Adds the user with the given ID to the tournament with the given ID
     * @param tournamentId
     * @param userId
     * @throws UserAlreadyRegisteredException
     */
    public void addUserToTournament(int tournamentId, int userId) throws UserAlreadyRegisteredException {
        SimpleJdbcInsert insertTournamentRegistration =
                new SimpleJdbcInsert(getDataSource())
                    .withTableName("tournamentregistrations");

        Map<String, Object> parameters = new HashMap<String, Object>(2);
        parameters.put("tournamentId", tournamentId);
        parameters.put("userId", userId);

        try {
            insertTournamentRegistration.execute(parameters);
        } catch (DataIntegrityViolationException divex) {
            String msg = "User already registered in tournament";
            log.warning(msg);
            throw new UserAlreadyRegisteredException(msg);
        }
    }

    /**
     * Returns the users registered in the tournament with the given ID.
     * @param tournamentId
     * @return
     */
    public List<User> getUsersInTournament(int tournamentId) {
        // join tournament registrations with the users table
        String sql = "select * from tournamentregistrations tr " +
                "join users u on tr.userId = u.id " +
                "where tr.tournamentId = ?";
        JdbcTemplate queryUsersInTournament = new JdbcTemplate(getDataSource());
        List<User> users = queryUsersInTournament.query(sql,
                new Object[] { tournamentId }, new TournamentRegistrationRowMapper());

        return users;
    }

    /**
     * Returns a list of active fantasy teams for the user with the given
     * userId
     * @param userId
     * @return
     */
    public List<FantasyTeamListItem> getActiveFantasyTeams(int userId) {
        String sql = "select * from tournamentregistrations tr " +
                "join tournaments t on tr.tournamentid = t.id " +
                "where tr.userid = ? and t.status = ?";

        JdbcTemplate queryFantasyTeams = new JdbcTemplate(getDataSource());
        List<FantasyTeamListItem> fantasyTeams = queryFantasyTeams.query(sql,
                new Object[]{ userId, TournamentStatus.STATUS_ACTIVE.ordinal() },
                new FantasyTeamListItemRowMapper());

        return fantasyTeams;
    }

    public void updateFantasyTeamPoints(int userId, int tournamentId, float points) {
        // TODO: update fantasy team points
        String sql = "update tournamentregistrations " +
                    "set teamPoints = ? " +
                    "where userId = ? and tournamentId = ?";

        JdbcTemplate updateFantasyTeamPoints = new JdbcTemplate(getDataSource());

        updateFantasyTeamPoints.update(sql, new Object[] { points, userId, tournamentId });
    }

    /**
     * Updates the fantasy team name of the
     * @param userId
     * @param tournamentId
     * @param name
     */
    public void updateFantasyTeamName(int userId, int tournamentId, String name) {
        String sql = "update tournamentregistrations " +
                "set teamName = ? " +
                "where userId = ? and tournamentId = ?";

        JdbcTemplate updateFantasyTeamName = new JdbcTemplate(getDataSource());

        updateFantasyTeamName.update(sql, new Object[] { name, userId, tournamentId });
    }
}
