package is.rufan.tournament.data;

/**
 * This exception gets thrown when a player is added to a fantasy team, but the player
 * is already in the fantasy team.
 * Created by Snaebjorn on 10/24/2015.
 */
public class PlayerAlreadySelectedException extends RuntimeException {
    public PlayerAlreadySelectedException() {
    }

    public PlayerAlreadySelectedException(String message) {
        super(message);
    }

    public PlayerAlreadySelectedException(String message, Throwable cause) {
        super(message, cause);
    }
}
