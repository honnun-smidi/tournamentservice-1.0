package is.rufan.tournament.data;

import is.rufan.tournament.domain.FantasyTeam;
import is.rufan.tournament.domain.Tournament;
import is.rufan.tournament.domain.TournamentStatus;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * This mapper is used to create fantasy teams from rows resulting from
 * joins between tournament registrations and tournaments
 * Created by Snaebjorn on 10/25/2015.
 */
public class FantasyTeamRowMapper implements RowMapper<FantasyTeam> {
    public FantasyTeam mapRow(ResultSet rs, int i) throws SQLException {
        FantasyTeam fantasyTeam = new FantasyTeam();
        fantasyTeam.setUserId(rs.getInt("userid"));
        fantasyTeam.setPoints(rs.getFloat("teamPoints"));
        fantasyTeam.setTeamName(rs.getString("teamName"));

        // get tournament from row data
        Tournament tournament = new Tournament();
        tournament.setId(rs.getInt("id"));
        tournament.setName(rs.getString("name"));
        tournament.setStartTime(rs.getDate("startTime"));
        tournament.setEndTime(rs.getDate("endTime"));
        tournament.setLeagueName(rs.getString("leagueName"));
        TournamentStatus tournamentStatus = TournamentStatus.fromInt(rs.getInt("status"));

        tournament.setStatus(tournamentStatus);

        fantasyTeam.setTournament(tournament);

        return fantasyTeam;
    }
}
